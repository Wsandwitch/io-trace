# Usage
# =====
#
# 1. Compiling for POSIX platforms OCPB, Linux, etc...
# $ make
# or
# $ make PLATFORM=posix
#
# 2. Compiling for Win32
# $ make PLATFORM=win32
#
# HINT: Default platform is POSIX

PLATFORM := posix

ARCH=x64
PREFIX=
CFLAGS=-static -Wall -Werror -g -I./src/cli -I./src/cli/uri/include/uriparser -I./src/cli/uri/include -I./src/cli/json/include -D__linux__

ifeq ($(PLATFORM),win32)
	LDFLAGS=-lpthread -lws2_32
else
    LDFLAGS=-lpthread
endif

OUTPUTPATH=debug
OUTPUTLIBPATH=$(OUTPUTPATH)/lib
OUTPUTHEADERS=$(OUTPUTLIBPATH)/include
SOURCES=$(wildcard src/cli/*.c src/cli/json/src/*.c src/cli/uri/src/*.c)
TESTSOURCES=$(wildcard src/t/*.c)
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))
LIBSOURCES=$(filter-out src/cli/cli.c,$(SOURCES))
LIBOBJECTS=$(patsubst %.c,%.o,$(LIBSOURCES))

TEST1SOURCES=$(filter-out src/t/mlcptests-02.c src/t/mlcptests-03.c src/t/mlcptests-04.c,$(TESTSOURCES))
TEST1OBJECTS=$(patsubst %.c,%.o,$(TEST1SOURCES))
TEST2SOURCES=$(filter-out src/t/mlcptests-01.c src/t/mlcptests-03.c src/t/mlcptests-04.c,$(TESTSOURCES))
TEST2OBJECTS=$(patsubst %.c,%.o,$(TEST2SOURCES))
TEST3SOURCES=$(filter-out src/t/mlcptests-01.c src/t/mlcptests-02.c src/t/mlcptests-04.c,$(TESTSOURCES))
TEST3OBJECTS=$(patsubst %.c,%.o,$(TEST3SOURCES))
TEST4SOURCES=$(filter-out src/t/mlcptests-01.c src/t/mlcptests-02.c src/t/mlcptests-03.c,$(TESTSOURCES))
TEST4OBJECTS=$(patsubst %.c,%.o,$(TEST4SOURCES))

LIBNAME=$(OUTPUTLIBPATH)/libmlcp-$(ARCH).a
TARGET=debug/cli
TEST1=debug/test1
TEST2=debug/test2
TEST3=debug/test3
TEST4=debug/test4

all: $(TARGET)
test1: $(TEST1)
test2: $(TEST2)
test3: $(TEST3)
test4: $(TEST4)

test: clean lib test1 test2 test3 test4

$(TARGET): build $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)

$(TEST1): build $(TEST1SOURCES)
	$(CC) -Wall -Werror -g -I./src/t -I$(OUTPUTHEADERS) $(TEST1SOURCES) -L$(OUTPUTLIBPATH) -lmlcp-x64 $(LDFLAGS) -o $@

$(TEST2): build $(TEST2SOURCES)
	$(CC) -Wall -Werror -g -I./src/t -I$(OUTPUTHEADERS) $(TEST2SOURCES) -L$(OUTPUTLIBPATH) -lmlcp-x64 $(LDFLAGS) -o $@

$(TEST3): build $(TEST2SOURCES)
	$(CC) -Wall -Werror -g -I./src/t -I$(OUTPUTHEADERS) $(TEST3SOURCES) -L$(OUTPUTLIBPATH) -lmlcp-x64 $(LDFLAGS) -o $@

$(TEST4): build $(TEST2SOURCES)
	$(CC) -Wall -Werror -g -I./src/t -I$(OUTPUTHEADERS) $(TEST4SOURCES) -L$(OUTPUTLIBPATH) -lmlcp-x64 $(LDFLAGS) -o $@

build:
	@mkdir -p $(OUTPUTPATH)

dev: CFLAGS+=-DNDEBUG
dev: all

clean:
	rm -rf ./debug ./src/t/*.o ./src/cli/*.o ./src/cli/uri/src/*.o ./src/cli/json/src/*.o *.dSYM

lib: $(LIBOBJECTS)
	@mkdir -p $(OUTPUTHEADERS)
	@cp -f src/cli/*.h $(OUTPUTHEADERS)
	@cp -f src/cli/uri/include/uriparser/*.h $(OUTPUTHEADERS)
	@cp -f src/cli/json/include/*.h $(OUTPUTHEADERS)
	@$(PREFIX)ar -r $(LIBNAME) $?
