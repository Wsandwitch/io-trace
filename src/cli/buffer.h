#ifndef BUFFER_H
#define BUFFER_H

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

struct Buffer {
    char *contents;
    int bytes_used;
    int total_size;
};

typedef struct Buffer Buffer;

extern Buffer * buffer_alloc(int initial_size);
extern int buffer_strlen(Buffer *buf);
extern void buffer_free(Buffer *buf);
extern int buffer_append(Buffer *buf, char *append, int length);
extern int buffer_appendf(Buffer *buf, const char *format, ...);
extern int buffer_nappendf(Buffer *buf, size_t length, const char *format, ...);
extern char *buffer_to_s(Buffer *buf);

#endif
