#ifndef CONNECT_H
#define CONNECT_H

#ifndef _WIN32
    #include <netdb.h>
    #include <arpa/inet.h>
#else
//    #include <windows.h>
    # include <ws2tcpip.h>
//    # ifdef __MINGW32__
//    WINSOCK_API_LINKAGE const char WSAAPI inet_ntop(
//            int af, const void *src, char *dst, socklen_t size);
//    # endif
#endif

#include <unistd.h>
#include <string.h>

#include <errno.h>
#include <fcntl.h>

#include <buffer.h>
#include <dbg.h>

#define STAT_OK      0
#define STAT_ER     -1
#define RECV_SIZE    65536
#define BUF_SIZE     RECV_SIZE + 1

typedef struct addressinfo {
    int                 ai_flags;
    int                 ai_family;
    int                 ai_socktype;
    int                 ai_protocol;
    socklen_t           ai_addrlen;
    struct sockaddr_in *ai_addr;
} addressinfo;

int fetch_response(int sockfd, Buffer **response, int recv_size);
int make_connection(char *server_addr, char * port);
int make_request(
    int sockfd, char *hostname, char *request_path, int type, char * post_data);

#endif
