#ifndef __REST_MCLP_H
#define __REST_MCLP_H

#include <errno.h>

#ifdef _WIN32
    #include <sys/stat.h>
#endif

#include <semaphore.h>
#include <stddef.h>
#include <string.h>
#include <time.h>

#include <buffer.h>
#include <connect.h>
#include <cJSON.h>

// 50 ms
#define NANOSLEEPVAL 50000000
#define TOUTATTEMPTS 10
#define READMODE     0
#define WRITEMODE    1
#define DEBUG_PRINTS 0
#define TAG_LEN      512

#define ARRAY_SIZE(a) ((int)(sizeof(a) / sizeof((a)[0])))

typedef struct comm {
    int    com;
    double val;
} comm;

typedef struct tag {
    char   tagname[TAG_LEN];
    double val;
} tag;

typedef struct mlcp_tags {
    sem_t        data_sync_sem;
    unsigned int controller_id;
    int          status;
    int          len;
    struct tag * tags;
    struct comm  command;
} mlcp_tags;

mlcp_tags * rest_mlcp_init_tags( unsigned int cntrl_id, char *tagnames[], int len );
void        rest_mlcp_free_tags( struct mlcp_tags * mlcp_t );
void        rest_mlcp_print_tags( struct mlcp_tags * mlcp_t );
char      * serialize_read_request( struct mlcp_tags * mlcp_t );
char      * serialize_write_request( struct mlcp_tags * mlcp_t );
char      * serialize_command_request( struct mlcp_tags * mlcp_t );
void        parse_read_response( char * content, struct mlcp_tags * mlcp_t );
void        parse_write_response( char * content, struct mlcp_tags * mlcp_t );
double      rest_mlcp_access_tag( struct mlcp_tags * mlcp_t, char *tagname, unsigned int access_mode, double value );


#endif // __REST_MCLP_H
