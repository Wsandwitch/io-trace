#include <request.h>

int
rest_mlcp_request(
    struct mlcp_tags * mlcp_t, char * server_addr, Url *url, int reqtype )
{
    char * content   = NULL;
    int sockfd       = 0;
    int status       = 0;
    Buffer *response = buffer_alloc(BUF_SIZE);

    sockfd = make_connection(server_addr,url->port);
    error_unless(
        sockfd > 0,
        "***ERR: connection failure <%s:%s>", server_addr, url->port);
    if ( DEBUG_PRINTS ) {
        if (reqtype) {
            printf("%s\n", serialize_write_request( mlcp_t ));
        }
        else {
            printf("%s\n", serialize_read_request( mlcp_t ));
        }
    }

    status = make_request(
        sockfd, url->hostname,
        url->path,
        POSTMETHOD,
        reqtype ?
            ( reqtype == 1 ?
                  serialize_write_request( mlcp_t ) :
                      serialize_command_request( mlcp_t )
            ) :
            serialize_read_request( mlcp_t )
    );
    if ( status > 0 ) {
        status = fetch_response(sockfd, &response, RECV_SIZE);
        error_unless(status >= 0, "***ERR: fetching response failed");

        char * body_prt = strstr( response->contents, HTTPDELIM) ;
        if ( body_prt != NULL ) {
            unsigned int body_sz = strlen( body_prt );
            if ( body_sz > strlen(HTTPDELIM) ) {
                content =
                    &( body_prt )[strlen(HTTPDELIM)];
                if ( content != NULL ) {
                    if ( strlen(content) > 0 ) {
                        if ( DEBUG_PRINTS )
                        {
                            printf("***DUM: %s\n", content);
                        }
                        if (reqtype == 1)
                        {
                            parse_write_response( content, mlcp_t );
                        }
                        else
                        {
                            parse_read_response( content, mlcp_t );
                        }
                    }
                }
            }
        }
        close(sockfd);
        buffer_free(response);
        return 0;
    }
error:
    if (sockfd > 0)  { close(sockfd); }
    buffer_free(response);
    return 1;
}


int
rest_mlcp_get_ctrl_tags(
    struct mlcp_tags * mlcp_t, char * server_addr, Url *url )
{
    int rc = rest_mlcp_request( mlcp_t, server_addr, url, READREQUEST );
    return rc;
}

int
rest_mlcp_set_ctrl_tags(
    struct mlcp_tags * mlcp_t, char * server_addr, Url *url )
{
    int rc = rest_mlcp_request( mlcp_t, server_addr, url, WRITEREQUEST );
    return rc;
}

int
rest_mlcp_command(
    struct mlcp_tags * mlcp_t, char * server_addr, Url *url )
{
    int rc = rest_mlcp_request( mlcp_t, server_addr, url, COMMREQUEST );
    return rc;
}
