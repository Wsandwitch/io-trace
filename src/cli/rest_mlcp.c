#include <rest_mlcp.h>


struct mlcp_tags *
rest_mlcp_init_tags( unsigned int cntrl_id, char *tagnames[], int len )
{
    int i;
    struct mlcp_tags    * mlcp_t   = NULL;
    struct tag          * tags     = NULL;
#ifdef _WIN32   
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
//    printf("WSAStartup failed with error: %d\n %d\n", err, WSAGetLastError());
    if (err != 0) {
        /* Tell the user that we could not find a usable */
        /* Winsock DLL.                                  */
        printf("WSAStartup failed with error: %d\n %d\n", err, WSAGetLastError());
    }
#endif   
    mlcp_t = calloc( 1, sizeof(mlcp_tags) );
    if ( sem_init ( &mlcp_t->data_sync_sem, 0, 1 ) != 0) {
        fprintf(
            stderr,
            "***WRN: sem_init failed %d {%s}\n",
            errno,
            strerror(errno)
        );
    }
    if ( len > 0 ) {
        tags   = calloc( len, sizeof(tag) );
        if ( mlcp_t != NULL && tags != NULL) {
            mlcp_t->len = len;
            mlcp_t->controller_id = cntrl_id;
            for( i=0; i<len; i++ ) {
                strcpy( tags[i].tagname, tagnames[i] );
            }
            mlcp_t->tags = tags;
        }
    }
    return mlcp_t;
}

void
rest_mlcp_free_tags( struct mlcp_tags * mlcp_t )
{
    if ( mlcp_t != NULL ) {
        if ( mlcp_t->tags != NULL ) {
            free( mlcp_t->tags );
        }
        if ( sem_destroy(&mlcp_t->data_sync_sem) != 0 ) {
            fprintf(
                stderr,
                "***WRN: sem_destroy failed %d {%s}\n",
                errno,
                strerror(errno)
            );
        }
        free(mlcp_t);
    }
#ifdef _WIN32   
    WSACleanup();
#endif

}

void
rest_mlcp_print_tags( struct mlcp_tags * mlcp_t )
{
    int i;
    if ( mlcp_t != NULL ) {
        for( i=0; i < mlcp_t->len; i++ ) {
            printf(
                "\t%03d: %-16s => %024.05f\n",
                i,
                mlcp_t->tags[i].tagname,
                (double)mlcp_t->tags[i].val
            );
        }
    }
}

char *
serialize_read_request( struct mlcp_tags * mlcp_t )
{
    int index;
    char  *string = NULL;
    cJSON *cid    = NULL;
    cJSON *tags   = NULL;

    cJSON *request = cJSON_CreateObject();
    if (request == NULL || mlcp_t == NULL ) {
        goto end;
    }
    cid = cJSON_CreateNumber(mlcp_t->controller_id);
    if (cid == NULL) {
        goto end;
    }
    cJSON_AddItemToObject(request, "controller_id", cid);
    cJSON *r_request = cJSON_CreateObject();
    if (r_request == NULL) {
        goto end;
    }
    cJSON_AddItemToObject(request, "r_request", r_request);
    tags = cJSON_CreateArray();
    if (tags == NULL) {
        goto end;
    }
    cJSON_AddItemToObject(r_request, "tags", tags);
    for (index = 0; index < mlcp_t->len; ++index) {
        cJSON *tag = cJSON_CreateString( mlcp_t->tags[index].tagname );
        if (tag == NULL) {
            goto end;
        }
        cJSON_AddItemToArray(tags, tag);
    }
    string = cJSON_Print(request);
    if (string == NULL) {
        fprintf(stderr, "Failed to print request JSON.\n");
    }
end:
    if ( request != NULL ) {
        cJSON_Delete(request);
    }
    return string;
}

char *
serialize_command_request( struct mlcp_tags * mlcp_t )
{
    char  *string  = NULL;
    cJSON *cid     = NULL;
    cJSON *comm    = NULL;
    cJSON *comid   = NULL;
    cJSON *comdat  = NULL;
    cJSON *request = NULL;

    if ( sem_trywait(&mlcp_t->data_sync_sem) != 0 ) {
        mlcp_t->status = -1;
        fprintf(
            stderr,
            "***WRN: sem_trywait failed COMM %d {%s}\n",
            errno,
            strerror(errno)
        );
        goto end;
    }
    else {
        request = cJSON_CreateObject();
        if (request == NULL || mlcp_t == NULL ) {
            goto end;
        }
        cid = cJSON_CreateNumber(mlcp_t->controller_id);
        if (cid == NULL) {
            goto end;
        }
        cJSON_AddItemToObject(request, "controller_id", cid);
        cJSON *r_request = cJSON_CreateObject();
        if (r_request == NULL) {
            goto end;
        }
        cJSON_AddItemToObject(request, "r_request", r_request);
        comm = cJSON_CreateObject();;
        if (comm == NULL) {
            goto end;
        }
        comid = cJSON_CreateNumber(mlcp_t->command.com);
        if (comid == NULL) {
            goto end;
        }
        comdat = cJSON_CreateNumber(mlcp_t->command.val);
        if (comdat == NULL) {
            goto end;
        }
        cJSON_AddItemToObject(comm, "comm_id", comid);
        cJSON_AddItemToObject(comm, "data", comdat);
        cJSON_AddItemToObject(r_request, "command", comm);
        string = cJSON_Print(request);
        if (string == NULL) {
            fprintf(stderr, "Failed to print request JSON.\n");
        }
        if (sem_post( &mlcp_t->data_sync_sem) != 0 ) {
            fprintf(
                stderr,
                "***WRN: sem_post failed %d {%s}\n",
                errno,
                strerror(errno)
            );
        }
    }
end:
    if ( request != NULL ) {
        cJSON_Delete(request);
    }
    return string;
}

char *
serialize_write_request( struct mlcp_tags * mlcp_t )
{
    int index;
    char  *string  = NULL;
    cJSON *cid     = NULL;
    cJSON *tags    = NULL;
    cJSON *request = NULL;

    if ( mlcp_t != NULL ) {
        if ( sem_trywait(&mlcp_t->data_sync_sem) != 0 ) {
            mlcp_t->status = -1;
            fprintf(
                stderr,
                "***WRN: sem_trywait failed %d {%s}\n",
                errno,
                strerror(errno)
            );
            goto end;
        }
        else {
            request = cJSON_CreateObject();
            if (request == NULL || mlcp_t == NULL ) {
                goto end;
            }
            cid = cJSON_CreateNumber(mlcp_t->controller_id);
            if (cid == NULL) {
                goto end;
            }
            cJSON_AddItemToObject(request, "controller_id", cid);
            cJSON *w_request = cJSON_CreateObject();
            if (w_request == NULL) {
                goto end;
            }
            cJSON_AddItemToObject(request, "w_request", w_request);
            tags = cJSON_CreateArray();
            if (tags == NULL) {
                goto end;
            }
            cJSON_AddItemToObject(w_request, "tags", tags);
            for (index = 0; index < mlcp_t->len; ++index) {
                cJSON *tag = cJSON_CreateObject();
                if (tag == NULL) {
                    goto end;
                }
                cJSON *tagname = cJSON_CreateString( mlcp_t->tags[index].tagname );
                if (tag == NULL) {
                    goto end;
                }
                cJSON *tagval = cJSON_CreateNumber(mlcp_t->tags[index].val);
                if (tagval == NULL) {
                    goto end;
                }
                cJSON_AddItemToObject(tag, "tag", tagname);
                cJSON_AddItemToObject(tag, "value", tagval);
                cJSON_AddItemToArray( tags, tag );
            }
            string = cJSON_Print(request);
            if (string == NULL) {
                fprintf(stderr, "Failed to print request JSON.\n");
            }
            if (sem_post( &mlcp_t->data_sync_sem) != 0 ) {
                fprintf(
                    stderr,
                    "***WRN: sem_post failed %d {%s}\n",
                    errno,
                    strerror(errno)
                );
            }
        }
    }
end:
    if ( request != NULL ) {
        cJSON_Delete(request);
    }
    return string;
}

void
parse_read_response(char * content, struct mlcp_tags * mlcp_t )
{
    int index;
    const cJSON *respstat   = NULL;
    const cJSON *r_response = NULL;
    const cJSON *tags       = NULL;
    const cJSON *tag        = NULL;
    cJSON *response         = NULL;

    if ( content != NULL ) {
        response = cJSON_Parse(content);
    }
    if ( response != NULL && mlcp_t != NULL ) {
        if ( sem_trywait(&mlcp_t->data_sync_sem) != 0 ) {
            mlcp_t->status = -1;
            fprintf(
                stderr,
                "***WRN: sem_trywait failed %d {%s}\n",
                errno,
                strerror(errno)
            );
            cJSON_Delete(response);
            return;
        }
        else {
            respstat = cJSON_GetObjectItemCaseSensitive(response, "status");
            if (cJSON_IsNumber(respstat)) {
                mlcp_t->status = respstat->valueint;
            }
            else{
                mlcp_t->status = -1;
                cJSON_Delete(response);
                return;
            }
            r_response = cJSON_GetObjectItemCaseSensitive(response, "r_response");
            tags = cJSON_GetObjectItemCaseSensitive(r_response, "tags");
            cJSON_ArrayForEach(tag, tags) {
                cJSON *tagname = cJSON_GetObjectItemCaseSensitive(tag, "tag");
                if (cJSON_IsString(tagname) && (tagname->valuestring != NULL)) {
                    cJSON *tagval = cJSON_GetObjectItemCaseSensitive(tag, "value");
                    if ( cJSON_IsNumber(tagval) ) {
                        for (index = 0; index < mlcp_t->len; ++index) {
                            if (
                                strcmp(
                                    mlcp_t->tags[index].tagname,
                                    tagname->valuestring
                                ) == 0
                            )
                            {
                                mlcp_t->tags[index].val = tagval->valuedouble;
                            }
                        }
                    }
                }
            }
            cJSON_Delete(response);
            if (sem_post( &mlcp_t->data_sync_sem) != 0 ) {
                fprintf(
                    stderr,
                    "***WRN: sem_post failed %d {%s}\n",
                    errno,
                    strerror(errno)
                );
            }
        }
    }
    else {
        if ( mlcp_t != NULL ) {
            mlcp_t->status = -1;
        }
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL) {
            fprintf(stderr, "***ERR: error before: %s\n", error_ptr);
        }
        if ( response != NULL ) {
            cJSON_Delete(response);
        }
    }
}

void
parse_write_response(char * content, struct mlcp_tags * mlcp_t )
{
    const cJSON *respstat = NULL;
    cJSON *response       = NULL;
    if ( content != NULL ) {
        response = cJSON_Parse(content);
    }
    if ( response != NULL && mlcp_t != NULL ) {
        respstat = cJSON_GetObjectItemCaseSensitive(response, "status");
        if (cJSON_IsNumber(respstat)) {
            mlcp_t->status = respstat->valueint;
        }
        else{
            mlcp_t->status = -1;
        }
        cJSON_Delete(response);
    }
    else {
        if ( mlcp_t != NULL ) {
            mlcp_t->status = -1;
        }
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL) {
            fprintf(stderr, "***ERR: error before: %s\n", error_ptr);
        }
        if ( response != NULL ) {
            cJSON_Delete(response);
        }
    }
    return;
}

double
rest_mlcp_access_tag(
    struct mlcp_tags * mlcp_t,
    char *tagname,
    unsigned int access_mode,
    double value
) {
    double rc = -1;
    int    i;
    if ( mlcp_t != NULL ) {
        if ( sem_trywait(&mlcp_t->data_sync_sem) != 0 ) {
            fprintf(
                stderr,
                "***WRN: sem_trywait failed %d {%s}\n",
                errno,
                strerror(errno)
            );
        }
        else {
            if ( tagname != NULL ) {
                if ( strlen(tagname) < TAG_LEN ) {
                    for ( i = 0; i < mlcp_t->len; i++ ) {
                        if ( mlcp_t->tags != NULL ) {
                            if (
                                strcmp(
                                    mlcp_t->tags[i].tagname,
                                    tagname
                                ) == 0
                            ) {
                                if ( access_mode == WRITEMODE ) {
                                    mlcp_t->tags[i].val = value;
                                }
                                else {
                                    rc = mlcp_t->tags[i].val;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            if (sem_post( &mlcp_t->data_sync_sem) != 0 ) {
                fprintf(
                    stderr,
                    "***WRN: sem_post failed %d {%s}\n",
                    errno,
                    strerror(errno)
                );
            }
        }
    }
    return rc;
}
