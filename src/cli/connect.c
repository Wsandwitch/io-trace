#include <rest_mlcp.h>


int
make_connection(char * server_addr, char * port)
{
    struct timespec dlay_w;
    struct timespec dlay_s = { 0, NANOSLEEPVAL };
    int connected = 0;
    int inx, err;
    struct sockaddr_in RemoteServerAddr;
    int ClientSock = socket(AF_INET, SOCK_STREAM, 0);
#ifdef _WIN32
    u_long iMode = 0;
    ioctlsocket(ClientSock, FIONBIO, &iMode);
#else
    fcntl(ClientSock, F_SETFL, O_NONBLOCK);
#endif
    for (inx = 0; inx < TOUTATTEMPTS; inx++) {
        if (ClientSock == STAT_ER) {
            if ( DEBUG_PRINTS ) {
                printf("***ERR: %s:%d socket error [%s]\n",
                    __FILE__, __LINE__, strerror(errno));
            }
        } else {
        #ifndef __linux__
            RemoteServerAddr.sin_len         = sizeof(RemoteServerAddr);
        #endif
            RemoteServerAddr.sin_family      = AF_INET;
            RemoteServerAddr.sin_port        = htons(atoi(port));
            RemoteServerAddr.sin_addr.s_addr = inet_addr(server_addr);
            err = connect(ClientSock,
                (struct sockaddr *) &RemoteServerAddr,
                sizeof(RemoteServerAddr));
        #ifndef _WIN32
            if ((err == STAT_ER) && (errno == EISCONN)) {
        #else
            printf("***INF: last WIN32 error: %d\n", WSAGetLastError());
            if (err==SOCKET_ERROR && WSAGetLastError() == WSAEISCONN) {//WSAEWOULDBLOCK){
        #endif
                if ( DEBUG_PRINTS ) {
                    printf("***INF: connected to %s:%u\n",
                        server_addr, atoi(port));
                }
                connected = 1;
                break;
            } else {
                if ( DEBUG_PRINTS ) {
                    printf( "***ERR: %s:%d timeout %s:%u [%s]\n",
                        __FILE__, __LINE__, server_addr,
                        atoi(port), strerror(errno));
                }
                //sleep(1);
                nanosleep(&dlay_s, &dlay_w);
           }
        } //socket OK
    } // for
    if ( !connected ) {
    #ifndef _WIN32
        close(ClientSock);
    #else
        closesocket(ClientSock);
    #endif
        ClientSock = -1;
    }
    return ClientSock;
}

char *
build_get_request(char *hostname, char *request_path)
{
    char *request = NULL;
    Buffer *request_buffer = buffer_alloc(BUF_SIZE);

    buffer_appendf(request_buffer, "GET %s HTTP/1.0\r\n", request_path);
    buffer_appendf(request_buffer, "Host: %s\r\n", hostname);
    buffer_appendf(request_buffer, "Connection: close\r\n\r\n");

    request = buffer_to_s(request_buffer);
    buffer_free(request_buffer);

    return request;
}

char *
build_post_request(char *hostname, char *request_path, char *post_data)
{
    char *request  = NULL;
    Buffer *request_buffer = buffer_alloc(BUF_SIZE);

    buffer_appendf(request_buffer, "POST %s HTTP/1.0\r\n", request_path);
    buffer_appendf(request_buffer, "Host: %s\r\n", hostname);
    //buffer_appendf(request_buffer, "Content-Type: application/x-www-form-urlencoded\r\n");
    buffer_appendf(request_buffer, "Content-Type: application/json\r\n");
    buffer_appendf(request_buffer, "Content-Length: %d\r\n\r\n", strlen(post_data));
    buffer_appendf(request_buffer, "%s\r\n", post_data);

    request = buffer_to_s(request_buffer);
    buffer_free(request_buffer);

    return request;
}

int
make_request(int sockfd, char *hostname, char *request_path, int type, char * post_data)
{
    struct timespec dlay_w;
    struct timespec dlay_s  = { 0, NANOSLEEPVAL };
    char *request           = NULL;
    size_t bytes_sent       = 0;
    size_t total_bytes_sent = 0;
    int error               = 0;
    if (type) {
        //request = build_post_request(hostname, request_path, get_emulated_params());
        if ( post_data != NULL ) {
            request = build_post_request(hostname, request_path, post_data);
        }
    } else {
        request = build_get_request(hostname, request_path);
    }
    if ( request != NULL ) {
        size_t bytes_to_send = strlen(request);
        debug("Bytes to send: %d", bytes_to_send);
        while ( error < TOUTATTEMPTS ) {
            bytes_sent = send(
                sockfd,
                (request + total_bytes_sent),
                (bytes_to_send - total_bytes_sent),
                0
            );
            if ( bytes_sent > 0 ) {
                total_bytes_sent += bytes_sent;

                debug("Bytes bytes_sent: %d", bytes_sent);
                /*
                printf("***INF: Bytes total_bytes_sent: %d\n", total_bytes_sent);
                printf("***INF: Condition: %d \n", total_bytes_sent >= bytes_to_send);
                printf("***INF: error: %d \n", WSAGetLastError());
                */

                if (total_bytes_sent >= bytes_to_send) {
                    /*
                    printf(
                        "***INF: break total_bytes_sent=%d bytes_to_send=%d\n",
                        (int)total_bytes_sent,
                        (int)bytes_to_send
                    );
                    */
                    break;
                }
            }
            else {
                bytes_sent = 0;
                error++;
            #ifndef _WIN32
                fprintf(
                    stderr,
                    "***ERR: send failure errno=%d{%s}\n",
                    errno,
                    strerror(errno)
                );
            #else
                debug("send failure code=%d\n", WSAGetLastError());
            #endif
                nanosleep(&dlay_s, &dlay_w);
           }
        }
        free(request);
        if ( post_data != NULL ) {
            free(post_data);
        }
    }
    return total_bytes_sent;
}

int
fetch_response(int sockfd, Buffer **response, int recv_size)
{
    size_t bytes_received;
    int status = 0;
    char data[recv_size];

    debug("Receiving data ...");

    while (1) {
        bytes_received = recv(sockfd, data, RECV_SIZE, 0);
        if (bytes_received < 0 && errno == EAGAIN) {
            continue;
        }

        /*if (bytes_received == -1) {
            return -1;
        }*/

        if (bytes_received == 0) {
            return 0;
        }

        if (bytes_received > 0) {
            status = buffer_append(*response, data, bytes_received);
            if (status != 0) {
                fprintf(stderr, "Failed to append to buffer.\n");
                return -1;
            }
        }
    }
    debug("Finished receiving data.");
    return status;
}
