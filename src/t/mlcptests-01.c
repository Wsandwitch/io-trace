#include "mlcp.h"

#define CONTROLLER  574

int
main(int argc, char *argv[])
{
    int i;
    int rc      = 1;
    Url *url    = url_parse(URLPATHRD);
    int sensnum = get_sensors_num();
    printf("available sensors: %d (last <%s>)\n", sensnum, get_sensors_name(sensnum-1));
    srand(time(0));
    for( i=0; i<TESTITERS; i++) {
        int first_value = get_first();
        int length      = get_length(first_value);
        if ( length > 0 ) {
            char ** names = names_for_iter(first_value, length);
            if ( names != NULL ) {
                struct mlcp_tags * mlcp_t =
                    rest_mlcp_init_tags( CONTROLLER, names, length );
                if ( mlcp_t != NULL ) {
                    rc = rest_mlcp_get_ctrl_tags(mlcp_t, SRVADDR, url);
                    if (rc == 0 ) {
                        if ( mlcp_t->status > -1) {
                            printf(
                                "%08d: ***INF - %03d tags are fetched\n",
                                i,
                                mlcp_t->len
                            );
                            //rest_mlcp_print_tags(mlcp_t);
                        }
                        else {
                            fprintf(
                                stderr,
                                "***ERR: tags fetching failure\n"
                            );
                            return 1;
                        }
                    }
                    else {
                        fprintf(
                            stderr,
                            "***ERR: request failure\n"
                        );
                        return 1;
                    }
                    rest_mlcp_free_tags(mlcp_t);
                }
                else {
                    fprintf(
                        stderr,
                        "***ERR: control params struct is NULL\n"
                    );
                    return 1;
                }
                free_names(names, length);
            }
            else {
                fprintf(
                    stderr,
                    "***ERR: names array is NULL\n"
                );
                return 1;
            }
        }
        else {
            printf(
                "***INF: skipped random generated zero length (length=%d)\n",
                length
            );
        }
    }
    return 0;
}
