#include "mlcp.h"

#define CONTROLLER  575

int
main(int argc, char *argv[])
{
    int i;
    int rc      = 1;
    Url *url    = url_parse(URLPATHWR);
    int sensnum = get_sensors_num();
    printf("available sensors: %d (last <%s>)\n", sensnum, get_sensors_name(sensnum-1));
    srand(time(0));
    for( i=0; i<TESTITERS; i++) {
        int first_value = get_first();
        int length      = get_length(first_value);
        if ( length > 0 ) {
            char ** names = names_for_iter(first_value, length);
            if ( names != NULL ) {
                struct mlcp_tags * mlcp_t =
                    rest_mlcp_init_tags( CONTROLLER, names, length );
                if ( mlcp_t != NULL ) {
                    fill_tags_with_random_vals( mlcp_t );
                    rc = rest_mlcp_set_ctrl_tags(mlcp_t, SRVADDR, url);
                    if (rc == 0 ) {
                        if ( mlcp_t->status > -1) {
                            printf(
                                "%08d: ***INF - %03d tags are pushed\n",
                                i,
                                mlcp_t->len
                            );
                            //rest_mlcp_print_tags(mlcp_t);
                        }
                        else {
                            fprintf(
                                stderr,
                                "***ERR: tags pushing failure\n"
                            );
                            return 1;
                        }
                    }
                    else {
                        fprintf(
                            stderr,
                            "***ERR: request failure\n"
                        );
                        return 1;
                    }
                    rest_mlcp_free_tags(mlcp_t);
                }
                else {
                    fprintf(
                        stderr,
                        "***ERR: control params struct is NULL\n"
                    );
                    return 1;
                }
                free_names(names, length);
            }
            else {
                fprintf(
                    stderr,
                    "***ERR: names array is NULL\n"
                );
                return 1;
            }
        }
        else {
            printf(
                "***INF: skipped random generated zero length (length=%d)\n",
                length
            );
        }
    }
    return 0;
}
