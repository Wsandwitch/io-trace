#include "mlcp.h"

#define CONTROLLER  574
#define TAGS        5
#define INDEXWR     2
#define DEBUG_T     0

int
main(int argc, char *argv[])
{
    Url *url;
    int i,j;
    int rc      = 1;
    char *tagnames[TAGS] = { "DI_1", "DI_2", "ADC_1", "ADC_2", "TEMP" };
    srand(time(0));
    for( i=0; i<TESTITERS; i++) {
        struct mlcp_tags * mlcp_t =
            rest_mlcp_init_tags( CONTROLLER, tagnames, TAGS );
        if ( mlcp_t != NULL ) {
            url = url_parse(URLPATHWR);
            double *values = calloc(TAGS,sizeof(double));
            for ( j = 0; j < TAGS; j++ ) {
                double rnd = rand();
                values[j] = rnd * ( i + 1 ) / 1000 + (i + 1)*(j + 1);
                rest_mlcp_access_tag(
                    mlcp_t,
                    tagnames[j],
                    WRITEMODE,
                    values[j]
                );
            }
            for ( j = 0; j < TAGS; j++ ) {
                double value = rest_mlcp_access_tag(
                    mlcp_t,
                    tagnames[j],
                    READMODE,
                    0
                );
                if ( value != values[j] ) {
                    fprintf(
                        stderr,
                        "tag value mismatch (1) at %08d: %f!=%f\n",
                        i,
                        value,
                        values[j]
                    );
                    return 1;
                }
            }
            rc = rest_mlcp_set_ctrl_tags(mlcp_t, SRVADDR, url);
            if (rc == 0 ) {
                if ( mlcp_t->status > -1) {
                    if ( DEBUG_T ) {
                        printf(
                            "%08d: ***INF - %03d tags are pushed\n",
                            i,
                            mlcp_t->len
                        );
                        //rest_mlcp_print_tags(mlcp_t);
                    }
                }
                else {
                    fprintf(
                        stderr,
                        "***ERR: tags pushing failure\n"
                    );
                    return 1;
                }
            }
            else {
                fprintf(
                    stderr,
                    "***ERR: request failure\n"
                );
                return 1;
            }

            url = url_parse(URLPATHRD);
            rc = rest_mlcp_get_ctrl_tags(mlcp_t, SRVADDR, url);
            if (rc == 0 ) {
                if ( mlcp_t->status > -1) {
                    if ( DEBUG_T ) {
                        printf(
                            "%08d: ***INF - %03d tags are fetched\n",
                            i,
                            mlcp_t->len
                        );
                        //rest_mlcp_print_tags(mlcp_t);
                    }
                }
                else {
                    fprintf(
                        stderr,
                        "***ERR: tags fetching failure\n"
                    );
                    return 1;
                }
            }
            else {
                fprintf(
                    stderr,
                    "***ERR: request failure\n"
                );
                return 1;
            }

            for ( j = 0; j < TAGS; j++ ) {
                values[j] = rest_mlcp_access_tag(
                    mlcp_t,
                    tagnames[j],
                    READMODE,
                    0
                );
                if ( DEBUG_T ) {
                    printf(
                        "\t %-8s: %08.08f\n",
                        tagnames[j],
                        values[j]
                    );
                }
            }
            rest_mlcp_access_tag( mlcp_t, tagnames[INDEXWR], WRITEMODE, 0.535 );
            rest_mlcp_access_tag( mlcp_t, tagnames[TAGS-1], WRITEMODE, 12.009 );
            for ( j = 0; j < TAGS; j++ ) {
                double value = rest_mlcp_access_tag(
                    mlcp_t,
                    tagnames[j],
                    READMODE,
                    0
                );
                if ( value != values[j] ) {
                    if ( j == INDEXWR || j == TAGS-1 ) {
                        if (j == INDEXWR && value == 0.535) {
                            ;
                        }
                        else if (j == TAGS-1 && value == 12.009) {
                            ;
                        }
                        else {
                            fprintf(
                                stderr,
                                "tag value mismatch (2) at %08d: %f!=%f\n",
                                i,
                                value,
                                values[j]
                            );
                            return 1;
                        }
                    }
                }
            }
            if (values != NULL) {
                free(values);
            }
            rest_mlcp_free_tags(mlcp_t);
            printf( "%08d: ***INF - test OK\n", i );
        }
        else {
            fprintf(
                stderr,
                "***ERR: control params struct is NULL\n"
            );
            return 1;
        }
    }
    return 0;
}
