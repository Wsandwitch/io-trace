#ifndef __MLCP_H
#define __MLCP_H

#include <stdlib.h>
#include <stdio.h>

#include <dbg.h>
#include <request.h>

#define TESTITERS   1000000
#define SRVADDR     "127.0.0.100"
#define URLPATHRD   "http://scada.mac-webtech/request-r.html"
#define URLPATHWR   "http://scada.mac-webtech/request-w.html"

extern char ** names_for_iter(int start, int length);
extern void free_names( char ** names, int length );
extern void fill_tags_with_random_vals(struct mlcp_tags * mlcp_t);
extern int get_sensors_num(void);
extern char * get_sensors_name(int index);
extern int get_first(void);
extern int get_length(int first_value);

extern UriUriA * uri_parse(char *url);
extern void      rest_mlcp_free_tags( struct mlcp_tags * mlcp_t );
extern void      rest_mlcp_print_fetched_tags( struct mlcp_tags * mlcp_t );
extern int       rest_mlcp_get_ctrl_tags(
    struct mlcp_tags * mlcp_t, char * server_addr, Url *url );
extern struct mlcp_tags * rest_mlcp_init_tags(
    unsigned int cntrl_id, char *tagnames[], int len );
extern double rest_mlcp_access_tag(
    struct mlcp_tags * mlcp_t, char *tagname, unsigned int access_mode, double value );

#endif // __MLCP_H
