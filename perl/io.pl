#!/usr/bin/env perl

use strict;
use warnings;

use CGI::Carp qw(fatalsToBrowser);
use CGI;
use POSIX;
use JSON qw(encode_json);
use DBI;

my $json = './feed/sample.json';
my $post = './logs/post.log';
my $co   = CGI->new;
my %env_params = map { $_ => $co->url_param($_) } $co->url_param();
my %dbaccess = ( host => '127.0.0.1', db => 'idc', tab => 's0000330_tags', username => 'postgres', password => 'ospasswd');

if ( (exists($env_params{action})) && ($env_params{action} eq 'get' || $env_params{action} eq 'data') ) {
    my $dbh;
    my %params = (
        id => 1,
        timestamp => time,
        level => 892.339,
        volume => 8757.263,
        water => 0.234,
        temperature => 19.837,
        density => 985.229,
        dbconnection => 'offline'
    );
    eval {
        $dbh = DBI->connect("dbi:Pg:dbname=" . $dbaccess{db} . ";host=" . $dbaccess{host},
            $dbaccess{username},
            $dbaccess{password},
            {AutoCommit => 0, RaiseError => 1, PrintError => 0}
        )
    };
    if ( $@ ) {
       my $msg = $@;
        $msg =~ s/([\t\r\"\']+)|([\r\n]+$)//gi;
        $msg =~ s/\n/. /gi;
        $params{dbconnection} = 'err: ' . $msg;
    } else {
        my $query = "SELECT * FROM " . $dbaccess{tab} . " ORDER BY id DESC LIMIT 1;";
        my $sth   = $dbh->prepare($query);
        my $rv    = $sth->execute();
        if (!defined $rv) {
            $params{dbconnection} = 'online, but error while execute query: ' . $dbh->errstr;
        } else {
            if ( my $ref = $sth->fetchrow_hashref() ) {
                if (
                    ( $ref->{level} ne '' && $ref->{level} !~ /null/i ) &&
                    ( $ref->{volume} ne '' && $ref->{volume} !~ /null/i ) &&
                    ( $ref->{water} ne '' && $ref->{water} !~ /null/i ) &&
                    ( $ref->{temperature} ne '' && $ref->{temperature} !~ /null/i ) &&
                    ( $ref->{density} ne '' && $ref->{density} !~ /null/i )
                ) {
                    $params{level} = $ref->{level};
                    $params{volume} = $ref->{volume};
                    $params{water} = $ref->{water};
                    $params{temperature} = $ref->{temperature};
                    $params{density} = $ref->{density};
                    $params{dbconnection} = 'online';
                } else {
                    $params{dbconnection} = 'online, but params contain NULLs';
                }
            } else {
                $params{dbconnection} = 'online, but could not fetchrow_hashref()';
            }
        }
        $sth->finish();
        $dbh->disconnect();
    }
    print(
        $co->header( -type => 'application/json' ) .
        encode_json( \%params )
    );
    exit;
}

if ( (exists($env_params{action})) && ($env_params{action} eq 'post') ) {
    my $rc = 0;
    my @serial;
    my $params = $co->Vars;
    if ( defined($params) ) {
        while ( my ($key, $value) = each(%$params) ) {
            if ( $key && $value ) {
                push( @serial, $key . q{=} . $value);
            }
        }
        if ( @serial ) {
            open( my $fh, '>>', $post );
            $rc = print $fh localtime.":\t".$co->param()."\t".join( q{,}, @serial )."\n";
            close( $fh );
        }
    }
    print(
        $co->header( -type => 'application/json' )  .
        '{ "result": "' . ($rc == 1 ? 'success' : 'error' ) . '", "params": "'.$co->param().'" }'
    );
    exit;
}

if ( (exists($env_params{action})) && ($env_params{action} eq 'test') ) {
    my $test_json = '{}';
    if ( -e $json ) {
        open( my $fh, '<', $json );
        $test_json = join(q{}, <$fh>);
        close( $fh );
    }
    print(
        $co->header( -type => 'application/json' ) . $test_json
    );
    exit;
}

print $co->header,$co->start_html;
print '<h4>UNDEFINED request to IO server</h4>';
while ( my ($key, $value) = each(%env_params) ) {
    print "<li>$key => $value</li>";
}
print $co->end_html;
