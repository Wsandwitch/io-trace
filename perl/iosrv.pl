#!/usr/bin/env perl

use strict;
use warnings;

use CGI::Carp qw(fatalsToBrowser);
use CGI;
use POSIX;
use JSON qw(encode_json decode_json);
use JSON::Validator;
use Encode qw( encode decode encode_utf8 decode_utf8);
use Data::Dumper;

my $co   = CGI->new;
my $jv   = JSON::Validator->new;
my %env_params = map { $_ => $co->url_param($_) } $co->url_param();
# my %dbaccess = ( host => '127.0.0.1', db => 'idc', tab => 's0000330_tags', username => 'postgres', password => 'ospasswd');

my %r_response = (
    status     => -1,
    message    => 'invalid request !!!!',
    r_response => { tags => [] }
);

if ( (exists($env_params{action})) && ($env_params{action} eq 'request-r') ) {
    my $cli_json;
    my @errors = ('no post data');
    $jv->schema('file://json_schemes/read_tag_comm_request.json');
    if ( defined $co->param("POSTDATA") ) {
        $cli_json = decode_json( encode_utf8($co->param("POSTDATA")) );
        @errors   = $jv->validate( $cli_json );
    }
    if ( !@errors ) {
        my @r_tags;
        if ( defined $cli_json->{'r_request'}->{'tags'} ) {
            foreach ( @{$cli_json->{'r_request'}->{'tags'}} ) {
                push(
                    @r_tags,
                    # { tag => $_, value => int(rand(200)), mode => 'r' }
                    {
                        tag   => $_,
                        value => sprintf("%.3f", rand(200))*1,
                        mode  => 'r'
                    }
                );
            }
        }
        else {
            # command
            open my $fh, ">>", "logs/command.log";
            print $fh
                localtime . "\t" .
                $ENV{REMOTE_ADDR}. "\t" .
                $cli_json->{'r_request'}->{'command'}->{'comm_id'} . "\t" .
                $cli_json->{'r_request'}->{'command'}->{'data'} . "\n";
            close $fh;
        }
        %r_response = (
            status     => 0,
            message    => 'tags fetch ok',
            r_response => { tags => [ @r_tags ] }
        );
        $jv->schema('file://json_schemes/read_tag_response.json');
        my @e = $jv->validate( \%r_response );
        if ( !@e ) {
            print(
                $co->header( -type => 'application/json' ) .
                encode_json( \%r_response )
            );
        }
        else {
            print(
                $co->header( -type => 'application/json' ) .
                encode_json(
                    {
                        status     => -1,
                        message    => join( q{,}, @e ),
                        r_response => { tags => [] }
                    }
                )
            );
        }
    }
    else {
        print(
            $co->header( -type => 'application/json' ) .
            encode_json(
                {
                    status     => -1,
                    message    => join( q{,}, @errors ),
                    r_response => { tags => [] }
                }
            )
        );
    }
    exit;
}

if ( (exists($env_params{action})) && ($env_params{action} eq 'request-w') ) {
    my $cli_json;
    my @errors = ('no post data');
    $jv->schema('file://json_schemes/write_tag_request.json');
    if ( defined $co->param("POSTDATA") ) {
        $cli_json = decode_json( encode_utf8($co->param("POSTDATA")) );
        @errors   = $jv->validate( $cli_json );
    }
    if ( !@errors ) {
        my @tags;
        open my $fh, ">>", "logs/post.log";
#        foreach ( @{$cli_json->{'w_request'}->{'tags'}} ) {
#            push(@tags, '{' . $_->{tag} . ':' . $_->{value} . '}');
#        }
#        print $fh localtime." ($ENV{REMOTE_ADDR}):\n".join( q{,}, @tags )."\n----------\n";
        print $fh localtime."\t".$ENV{REMOTE_ADDR}."\t".( $#{$cli_json->{'w_request'}->{'tags'}} + 1 )."\n";
        close $fh;

        my %w_response = (
            status     => 0,
            message    => 'tags push ok'
        );
        $jv->schema('file://json_schemes/write_tag_response.json');
        my @e = $jv->validate( \%w_response );
        if ( !@e ) {
            print(
                $co->header( -type => 'application/json' ) .
                encode_json( \%w_response )
            );
        }
        else {
            print(
                $co->header( -type => 'application/json' ) .
                encode_json(
                    {
                        status     => -1,
                        message    => join( q{,}, @e )
                    }
                )
            );
        }
    }
    else {
        print(
            $co->header( -type => 'application/json' ) .
            encode_json(
                {
                    status     => -1,
                    message    => join( q{,}, @errors ),
                    w_response => { tags => [] }
                }
            )
        );
    }
    exit;
}

print(
    $co->header( -type => 'application/json' ) .
    encode_json( \%r_response )
);
