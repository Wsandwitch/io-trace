# IO-trace: REST-MLCP protocol minimalistic client library

This is minimalistic client library for REST-MLCP data transfer protocol.

## What is REST-MLCP?

REST-MLCP is application level JSON driven data transfer protocol for supervisory control and data acquisition (SCADA) industrial systems. JSON schemes are availabe for: [read request](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/perl/json_schemes/read_tag_comm_request.json), [write request](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/perl/json_schemes/write_tag_request.json), [response to read request](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/perl/json_schemes/read_tag_response.json) and [response to write request](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/perl/json_schemes/write_tag_response.json).

## IO-trace control structures

Basic **IO-trace** data handle [structure](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/src/cli/rest_mlcp.h#L31) `struct mlcp_tags` is:

```c++
typedef struct mlcp_tags {
    sem_t        data_sync_sem;
    unsigned int controller_id;
    int          status;
    int          len;
    struct tag * tags;
    struct comm  command;
} mlcp_tags;
```

It refers to secondary data handle [structures](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/src/cli/rest_mlcp.h#L21) `struct tag`, `struct comm` at **tags** and **command** members:

```c++
typedef struct tag {
    char   tagname[TAG_LEN];
    double val;
} tag;

typedef struct comm {
    int    com;
    double val;
} comm;
```

## IO-trace API

- `mlcp_tags * rest_mlcp_init_tags( unsigned int cntrl_id, char *tagnames[], int len );` — initialize basic **IO-trace** data handle structure with given tagnames;

- `void rest_mlcp_free_tags( struct mlcp_tags * mlcp_t );` — free basic **IO-trace** data handle structure;

- `void rest_mlcp_print_tags( struct mlcp_tags * mlcp_t );` — print tags with values for basic **IO-trace** data handle structure to [+&nbsp;STDOUT&nbsp;+];

- `double rest_mlcp_access_tag( struct mlcp_tags * mlcp_t, char *tagname, unsigned int access_mode, double value );` — safe access to tag value, **access_mode** argument should be [+&nbsp;1&nbsp;+] for read or [+&nbsp;0&nbsp;+] for write;

- `int rest_mlcp_get_ctrl_tags( struct mlcp_tags * mlcp_t, char * server_addr, Url *url );` — request values for given tagnames from IO server;

- `int rest_mlcp_set_ctrl_tags( struct mlcp_tags * mlcp_t, char * server_addr, Url *url );` — request IO server to store values for given tagnames;

- `int rest_mlcp_command( struct mlcp_tags * mlcp_t, char * server_addr, Url *url );` — send command (with data, see [comm](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/src/cli/rest_mlcp.h#L21) data structure) to IO server.

## Build IO-trace and its tests

**IO-trace** is dependable on [pthread](https://www.cs.cmu.edu/afs/cs/academic/class/15492-f07/www/pthreads.html) library, so you need it pre-installed.

Build library for **POSIX** platform:

```bash
$ make lib
```

Build tests:

```bash
$ make test
```

Build for **Win32** platform:

```bash
$ make PLATFORM=win32
```

```bash
$ make PLATFORM=win32 lib
```

```bash
$ make PLATFORM=win32 test
```

Library and test executables will store to `./debug` directory.

## Examples

Please look through [tests](https://gitlab.com/pheix-scada/io-trace/-/tree/devel/src/t) for usage examples and cases:

- multiple read requests/responces: [test no.1](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/src/t/mlcptests-01.c);

- multiple write requests/responces: [test no.2](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/src/t/mlcptests-02.c);

- multiple command transfers: [test no.3](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/src/t/mlcptests-03.c);

- generic usage test: [test no.4](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/src/t/mlcptests-04.c).

## Quick start

Quick start is supported by [pre-built](https://gitlab.com/pheix-scada/io-docker-scada/container_registry) **Docker** [container](https://gitlab.com/pheix-scada/io-docker-scada) with Apache HTTP server, PostgerSQL database and **IO-trace** [server-side SW](https://gitlab.com/pheix-scada/io-trace/-/tree/devel/perl): all you need is to install **Docker** package and run this container with shared to host network (`--net=host` commandline arg).

**Important!** Also you should [fix](https://gitlab.com/pheix-scada/io-trace/-/blob/devel/src/t/mlcp.h#L11) your server IP address and read/write request URLs at `#define` section in **IO-trace** test sources:

- `SRVADDR` - server IP address, if you're using pre-built **Docker** container, fix it to [+&nbsp;127.0.0.1&nbsp;+];

- `URLPATHRD` - read request URL, if you're using pre-built **Docker** container, fix it to http://127.0.0.1:8888/request-r.html;

- `URLPATHWR` - write request URL, if you're using pre-built **Docker** container, fix it to http://127.0.0.1:8888/request-w.html;

## License

**IO-trace** is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
